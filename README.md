Application to control NewFocus Velocity lasers.

This application is currently only compatible with Python 2, and only tested on Windows.

# Installing

After downloading this repository, first initialize the `webui` submodule with
```
git submodule init
git submodule update
```

Then, install all the required dependancies listed in `requirements.txt` with
```
py -2 -m pip install -r requirements.txt
```

Finally, the lasernet application can be started with
```
py -2 lasernet.py --addr GPIB:9
```
for communicating with GPIB connected lasers, or 
```
py -2 lasernet.py --67
```
for communicating with the USB enabled 6700 series of lasers.

Then open a webbrowser to http://localhost:1134 to access the UI.