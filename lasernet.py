from webui import *
import time

import labdrivers

import argparse
parser = argparse.ArgumentParser(description="Control NewFocus Velocity lasers")
parser.add_argument("--67", action="store_true", help="Useing 6700 series laser")
parser.add_argument("--trace", action="store_true", help="debug")
parser.add_argument("--addr", "-a", type=str, default="GPIB::9", help="GPIB dress")
args = parser.parse_args()


if args.__getattribute__("67"):
    import labdrivers.newport
    l = labdrivers.newport.Vl67()
    if not l.ok():
        raise RuntimeError("Could not find 6700 laser on usb")
else:
    import labdrivers.visaa
    l = labdrivers.visaa.Vl63(args.addr)
    if not l.ok():
       raise RuntimeError("couldn't find 6300 laser on %s" % args.addr)

import labdrivers.zmqq
qdaq = labdrivers.zmqq.QDaq()
sa = labdrivers.zmqq.SA()

def log(text):
    print(text)
    uis.status(text=text)

scanning = False

@gthread
def do_one_scan(start, stop, slew, save=False):
    global scanning
    scanning = True

    l.set_start(start)
    l.set_stop(stop)
    l.set_slew("MAX", "MAX")

    log("resetting...")
    l.reset()
    while scanning and not l.ready():
        sleep(.01)

    l.set_slew(slew, slew)
    sleep(0.2)
    qdaq.start()

    l.start()
    t0 = time.time()
    log("scanning...")

    while scanning and not l.ready():
        dt = time.time() - t0
        dwl = float(l.sense_wave()) - start
        if dt > 1. and abs(dwl) < 0.002:
            l.set_track(0)

        sleep(.01)

    qdaq.stop()

    if save is False:
        pass
    elif save is True:
        qdaq.save()
    else:
        qdaq.save(str(save))

    scanning = False
    log("done scan")

@gthread
def do_repeat_scan(start, stop, slew):
    global scanning
    scanning = True

    l.set_start(start)
    l.set_stop(stop)
    l.set_slew(slew, slew)

    while scanning:
        log("backward...")
        l.reset()
        while scanning and not l.ready():
            sleep(.01)

        l.start()
        log("forward...")

        while scanning and not l.ready():
            sleep(.01)
    log("stopped scanning")

@action
def scanonce(start, stop, slew, save=False):
    if not l.ok():
        ui.status(text="no laser to scan")
        return

    if scanning:
        ui.status(text="scan in progress")
        return

    do_one_scan(float(start), float(stop), float(slew), save=save)

@action
def scanrepeat(start, stop, slew):
    if not l.ok():
        ui.status(text="no laser to scan")
        return

    if scanning:
        ui.status(text="scan in progress")
        return

    do_repeat_scan(float(start), float(stop), float(slew))

@action
def stop():
    global scanning
    scanning = False
    l.stop()
    log("stopped")

def forw_range(start, stop, step):
    while start <= stop:
        yield start
        start += step

def revs_range(start, stop, step):
    while start >= stop:
        yield start
        start -= step

@gthread
def do_pz_scan(start, stop, step):
    if start < stop:
        rang = forw_range(start, stop, step)
    else:
        rang = revs_range(start, stop, step)

    l.set_piezo(start)
    sleep(1)

    qdaq.start()
    do_pz_scan.stop = False
    for v in rang:
        l.set_piezo(v)
        uis.piezo(now=v)
        sleep(0.001)

        if do_pz_scan.stop is True:
            break

    qdaq.stop()

@action
def pzscan(start, stop, step=.3):
    if not l.ok():
        ui.status(text="no laser to scan")
        return

    do_pz_scan(float(start), float(stop), float(step))

@action
def stop_pz_scan():
    do_pz_scan.stop = True

@action
def volt(volt):
    volt = float(volt)
    l.set_piezo(volt)
    uis.piezo(now=l.get_piezo())

@action
def get_volt():
    ui.piezo(now=l.get_piezo())



@action
def connected():
    ui.status(text="connected")

    if l.ok():
        ui.setscan(
            start=float(l.get_start()),
             stop=float(l.get_stop()),
             slew=float(l.get_slew_f()))
        uis.piezo(now=l.get_piezo())

    checklaser()
    checkpower()
    checkqdaq()



@action
def qdaqloc(loc):
    qdaq.connect(loc, 1619)
    uis.qdaq(connected=qdaq.ok(), server=loc)

@action
def shutdown():
    serv.stop()


#@every(10)
def checkqdaq():
    if qdaq.ok():
        uis.qdaq(connected=True, server=qdaq.server)
    else:
        uis.qdaq(connected=False)

@action
def saloc(loc):
    sa.connect(loc)
    uis.sa(connected=sa.ok(), server=loc)

@every(1)
@action
def checklaser():
    if l.ok():
        uis.laser(
            identity=l.identity(),
            current=float(l.sense_current()),
            wave=float(l.sense_wave()))
    else:
        uis.nolaser()

@action
def current(current):
    if l.ok():
        l.set_current(current)

@action
def poweron():
    if l.ok():
        l.set_power(True)
        checkpower()

@action
@gthread
def goto(wave):
    l.set_track(0)
    l.set_wave(wave)
    ui.ready(status=True)

@action
def ready():
    ui.ready(status=l.ready())

@action
def poweroff():
    if l.ok():
        l.set_power(False)
        checkpower()

@every(10)
def checkpower():
    if l.ok():
        uis.laserpower(power=l.powered())

@action
def interrupt():
    l.set_track(0)

if args.trace:
    import sys

    def trace(frame, event, arg):
        if frame.f_code.co_filename.find("lasernet") > 0:
            print("%s, %s:%d" % (event, frame.f_code.co_filename, frame.f_lineno))
        return trace

    sys.settrace(trace)

serv = server(1134)
print("starting server at http://localhost:1134")
serv.serve_forever()
