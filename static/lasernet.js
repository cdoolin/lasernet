$(function() {
//
// UI Events
//
//

    $('#interruptbutt').click(function() {
        webui.call("interrupt", {});
    });
        

    $('#waveupbutt').click(function() {
        var wave = parseFloat($("#goto").val())
        var skip = parseFloat($("#jumpby").val())
        console.log(wave);
        webui.call('goto', {wave: wave + skip});
    });

    $('#wavedownbutt').click(function() {
        var wave = parseFloat($("#goto").val())
        var skip = parseFloat($("#jumpby").val())
        webui.call('goto', {wave: wave - skip});
    });

    $('#scanbutt').click(function() {
        webui.call("scanonce", {
            start: $('#start').val(),
             stop: $('#stop').val(),
             slew: $('#slew').val(),
        });
    });
    $('#scanrepeatbutt').click(function() {
        webui.call("scanrepeat", {
            start: $('#start').val(),
             stop: $('#stop').val(),
             slew: $('#slew').val(),
        });
    });
    $('#stopbutt').click(function() {webui.call("stop");});

    $('#shutdown').click(function() {webui.call("shutdown");});

    $('#pscanbutt').click(function() {
        webui.call("pzscan", {
            start: $('#pstart').val(),
             stop: $("#pstop").val(),
        });
    });
    $('#pstopbutt').click(function() {webui.call("stop_pz_scan");});

    $('.heading').click(function() {
        var h = $(this);
        
        if(h.hasClass('isdown')) {
            h.next().slideUp();
            h.removeClass('isdown');
            h.addClass('isup');
        } else if (h.hasClass('isup')) {
            h.next().slideDown();
            h.removeClass('isup');
            h.addClass('isdown');
        }
    });

    $('#qdaqloc').keyup(function() {
        if(event.keyCode == 13) {
            webui.call('qdaqloc', {loc: $(this).val()});
            $(this).blur();
        }
    });

    $('.qdaq').click(function(event) {
        var e = $('#qdaqopts');
        if(e.is(':visible'))
            return;

        if($('#saopts').is(':visible'))
            $('#saopts').slideUp('fast');

        var offset = $(this).offset();
        offset.top += $(this).height();
        //var o = {top: offset.top + $(this).height(), left: offset.left};
        e.slideDown('fast');
        e.offset(offset);

        event.stopPropagation();
    });

    $('.sa').click(function(event) {
        var e = $('#saopts');
        if(e.is(':visible'))
            return;

        if($('#qdaqopts').is(':visible'))
            $('#qdaqopts').slideUp('fast');

        var offset = $(this).offset();
        offset.top += $(this).height();
        //var o = {top: offset.top + $(this).height(), left: offset.left};
        e.slideDown('fast');
        e.offset(offset);

        event.stopPropagation();
    });

    $('.power').click(function() {
        if($(this).hasClass('connected'))
            webui.call('poweroff');
        else
            webui.call('poweron');
    });

    $('#current').keyup(function(event) {
        if(event.keyCode == 13) {
            webui.call('current', {current: $(this).val()});
            $(this).blur();
        }
    });

    $('#goto').keyup(function(event) {
        if(event.keyCode == 13) {
            webui.call('goto', {wave: $(this).val()});
            $(this).blur();
        }
    });

    $('#qdaqopts').click(function(event) {
        event.stopPropagation();
    });

    $('html').click(function() {
        var e = $('#qdaqopts')
        if(e.is(':visible'))
            e.slideUp('fast');

        var e = $('#saopts')
        if(e.is(':visible'))
            e.slideUp('fast');
    });

    $('#pnow').keyup(function(event) {
        if(event.keyCode == 13) {
            webui.call('volt', {volt: $(this).val()});
            $(this).blur();
        }
    });
//
/// WebUI Actions
//

    webui.actions.status = function(args) {
        $("#status").text(args.text);
    };


    webui.actions.nolaser = function(args) {
        $('#ident').text("disconnected")
        $('#ident').addClass("disconnected")

        var p = $('.power');
        p.text('off');
        if(p.hasClass('connected'))
            p.removeClass('connected');
        p.addClass('disconnected');

        $('#current').val('');
        $('#goto').val('');

        $("#scanbutt").attr("disabled", "disabled");
        $("#scanrepeatbutt").attr("disabled", "disabled");
        $("#stopbutt").attr("disabled", "disabled");
    };

    webui.actions.qdaq = function(args) {
        if('server' in args)
            $('#qdaqloc').val(args.server)

        var e = $('.qdaq');
        if(!args.connected) {
            e.removeClass('connected');
            e.addClass('disconnected');
        } else if(args.connected) {
            e.removeClass('disconnected');
            e.addClass('connected');
        }
    };

    webui.actions.current = function(args) {
        var e = $('#current');
        if(!e.is(':focus'))
            e.val(args.current);
    };

    // webui.actions.wave = function(args) {
        // var e = $('#goto');
        // if(!e.is(':focus'))
            // e.val(args.wavelength);
    // };

    webui.onclose = function() {
        $("#status").text("disconnected");
    };
	
	webui.actions.laser = function(args) {
		if('identity' in args)
			$("#ident").text(args.identity);
		if('current' in args && !$("#current").is(":focus"))
			$("#current").val(args.current.toFixed(1));
		if('wave' in args && !$("#goto").is(":focus"))
			$("#goto").val(args.wave.toFixed(3));
	};

    webui.actions.piezo = function(args) {
		if('now' in args && !$("#pnow").is(":focus"))
			$("#pnow").val(args.now.toFixed(1));
    };
	
	webui.actions.laserpower = function(args) {
        var p = $('.power');
		if(args.power){
			p.text('on');
			if(p.hasClass('disconnected'))
				p.removeClass('disconnected');
			p.addClass('connected');
		} else {
			p.text('off');
			if(p.hasClass('connected'))
				p.removeClass('connected');
			p.addClass('disconnected');
		}
	}
	
	webui.actions.setscan = function(args) {
		$("#start").val(args.start.toFixed(1));
		$("#stop").val(args.stop.toFixed(1));
		$("#slew").val(args.slew.toFixed(2));
	}


    //var spinopts = {lines: 12, length: 5, width: 1, radius:2}
    //spin = Spinner(spinopts).spin(document.getElementById("spinner"));
});
